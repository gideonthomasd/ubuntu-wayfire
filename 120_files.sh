#!/bin/bash

mkdir -p ~/.config/rofi
mkdir -p ~/.config/kitty
mkdir -p ~/.config/waybar
mkdir -p ~/.local/share/fonts
mkdir -p ~/wallpaper
mkdir -p ~/.config/wayfire

cd rofi
cp -r * ~/.config/rofi
cd ..

cd kitty
cp -r * ~/.config/kitty
cd ..

cd waybar
cp -r * ~/.config/waybar
cd ..

cd wayfire
cp -r * ~/.config/wayfire
cd ..

cd fonts
cp -r * ~/.local/share/fonts
cd ..

cd wallpaper
cp -r * ~/wallpaper
cd ..

cp wayfire.ini ~/.config/wayfire.ini



