#!/bin/bash

sudo apt install -y kitty gparted htop gparted lxpolkit pamixer cliphist swaybg waybar audacious xfce4-terminal thunar wayfire curl xwayland wayland-protocols rofi dmenu pavucontrol geany playerctl
echo "Change audacious so a) it is in qt mode - else won't be in taskbar b) Change settings so it is in taskbar"
# sudo apt install -y pulseaudio
